package model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class CollectionTour extends LinkedHashSet <Tour> implements Serializable {
    private static CollectionTour listTours ;

    public int getidlasttour(){
        int id;
        if(listTours.size()==0){
            id=0;
        }
        else {
            Tour t = (Tour) listTours.toArray()[listTours.size() - 1];
            id = t.getIdTour();
        }
        return id;
    }
    public CollectionTour() {
    }

    public static CollectionTour getList(){
        if(listTours==null){
            listTours=new CollectionTour();
        }
            return listTours;
    }

    public static CollectionTour getInstance(){
        if(listTours == null){
            listTours = new CollectionTour();
            listTours.add(new Tour(1,"Air. Royal london","Sightseeing tour of London on a comfortable bus with a professional guide. Unknown and mysterious city: walking tour of the City area. Excursion to Westminster Abbey. Excursion to Windsor Castle."));
            listTours.add(new Tour(2,"Bus. Berlin in 4 days","Berlin is an ideal tourist destination. In this city, anyone will find entertainment to their taste and afford. Anyone whom you ask about which 2 features are most characteristic of Berlin will tell you that this is affordability and multiculturalism."));
            listTours.add(new Tour(3,"Air. Weekend in Amsterdam","Optional excursion: Walking tour of Amsterdam, including an evening visit to the Van Gogh Museum (4 hours) Optional excursions: Walking tour of Haarlem including a visit to the Frans Hals Museum with a Russian guide (5 hours)"));
        }
        return listTours;
    }

    public static int count(){
        return listTours.size();
    }
    public static boolean addTour(Tour tour){
        if(listTours == null){
            listTours=new CollectionTour();
        }
        listTours.add(tour);
        return true;
    }

    public static boolean delTour(int id){
        Iterator<Tour> iterator = listTours.iterator();
        while (iterator.hasNext())
        {
            Integer v = iterator.next().getIdTour();
            if (v == id) iterator.remove();
        }
        return true;
    }

    public static void main(String[] args) {

        CollectionTour tours= new CollectionTour();
        tours.add(new Tour(4, "fourth Tout", "fourth desc"));
        tours.add(new Tour(4, "fifth Tout", "fourth desc"));
        // myGarage.add(new Car(101, "XK12348", "Opel Vectra"));
        // System.out.println(myGarage);
        for (Tour tour : CollectionTour.getInstance()) {
            System.out.println(tour);
        }
        for (Tour tour : tours) {
            System.out.println(tour);
        }
    }

    public static void setList(CollectionTour line) {
        listTours=line;
    }

    @Override
    public String toString() {
        String res="";
        for( Tour t : listTours){
            res+=("\n"+t.toString());
        }
        return res;
    }
}
