package model;

import java.util.ArrayList;
import java.util.Objects;

public class Users {
    static ArrayList<User> users = new ArrayList();

    public Users(ArrayList<User> user) {
        this.users = user;
    }

    public Users() {
    }

    public ArrayList<User> getUser() {
        return users;
    }

    public void setUser(ArrayList<User> user) {
        this.users = user;
    }

    public static ArrayList<User> getInstance(){
        if(users == null){
            users = new ArrayList<>();
            users.add(new User("username1","password1"));
            users.add(new User("username2","password2"));
            users.add(new User("username3","password3"));
        }
        return users;
    }

    public static User findUser(String username, String password){
        User findU=new User();
        for (User user: users) {
            if (user.getPassword()==password&&user.getUsername()==username){
                findU=user;
            }
        }
        return findU;
    }

    public static boolean chek(String username,String password){
        boolean sw=false;
        if(("admin".equals(password))&&("admin".equals(username))) {
            sw=true;
        }
        return sw;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return Objects.equals(users, users.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(users);
    }
}
