package servlets;

import model.CollectionTour;
import model.Tour;
import org.w3c.dom.ls.LSOutput;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.Line;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedHashSet;

@WebServlet("/Save")
public class SaveServlet extends HttpServlet {
    public SaveServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        java.util.LinkedHashSet<Tour> saveT = (LinkedHashSet<Tour>) request.getAttribute("tourList");
        System.out.println(request.getAttribute("tourList"));
        System.out.println(request.getAttribute("listTour"));
        java.util.LinkedHashSet<model.Tour> listTour = model.CollectionTour.getInstance();
        FileOutputStream fos1 = new FileOutputStream("d:\\ser.xml");
        java.beans.XMLEncoder xe1 = new java.beans.XMLEncoder(fos1);
        xe1.writeObject(CollectionTour.getList());
        xe1.close();
        response.sendRedirect("PrintTour");
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
