package servlets;

import model.CollectionTour;
import model.Tour;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;

@WebServlet("/EditTour")
public class EditTourServlet extends HttpServlet {
    public EditTourServlet() {
        super();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = (String) request.getParameter("name");
        String desc = (String) request.getParameter("desc");
        int id = Integer.parseInt(request.getParameter("code"));
        Tour newTour = new Tour(id, name, desc);
        if (CollectionTour.addTour(newTour)) {
            newTour = null;
            response.sendRedirect("PrintTour");
        } else {
            request.setAttribute("tour", newTour);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTour.jsp");
            dispatcher.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Tour tour = new Tour(Integer.parseInt(request.getParameter("idTour")), (String) request.getParameter("nameTour"), (String) request.getParameter("descTour"));
        request.setAttribute("tour", tour);
        CollectionTour.delTour(tour.getIdTour());
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/EditTour.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
