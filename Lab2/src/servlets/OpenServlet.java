package servlets;

import model.CollectionTour;
import model.Tour;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.IOException;

@WebServlet("/Open")
public class OpenServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (XMLDecoder xmlDecoder = new XMLDecoder(new FileInputStream("d:\\ser.xml"))) {
            CollectionTour line = (CollectionTour) xmlDecoder.readObject();
            CollectionTour.setList(line);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        for (Tour tour : CollectionTour.getList()) {
            System.out.println(tour);
        }
        response.sendRedirect("PrintTour");
    }
}
