package servlets;


import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Tour;
import model.CollectionTour;
import model.Users;

@WebServlet("/PrintTour")
public class PrintTourServlet extends HttpServlet {
    public PrintTourServlet() {
        super();
    }

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Tour> tourList = new ArrayList<Tour>();
        Tour tour = new Tour();
        Tour tour1 = new Tour();
        tour.setIdTour(0);
        tour.setNameTour("A");
        tour.setDescTour("B");
        tour1.setIdTour(1);
        tour1.setNameTour("C");
        tour1.setDescTour("D");
        tourList.add(tour);
        tourList.add(tour1);

        java.util.LinkedHashSet<model.Tour> listTour = model.CollectionTour.getList();
        request.setAttribute("tourList", listTour);
        System.out.println("tourList in PrintTout..." + request.getAttribute("tourList"));
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/PrintTour.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
